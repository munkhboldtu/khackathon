
// SLIDE MARQUEE
class InfiniteMarque {
  constructor(payload, direction) {
    this.DOM = {
      marque: payload.element,
    };
    this.direction = direction;
    this.init();
  }

  init() {
    this.DOM.marque.forEach((marque) => {
      var marqueContent = marque.childNodes[1];
      var contentCloned = marqueContent.cloneNode(true);
      marque.append(contentCloned);
      this.playMarque(
        {
          marque: marque,
          content: marqueContent,
        },
        this.direction
      );
      window.addEventListener("resize", (e) => {
        this.playMarque(
          {
            marque: marque,
            content: marqueContent,
          },
          this.direction
        );
      });
    });
  }
  playMarque(payload, direction) {
    // calculate gap + total distance
    let gap = parseInt(
      getComputedStyle(payload.marque).getPropertyValue("column-gap"),
      10
    );
    let width = parseInt(
      getComputedStyle(payload.content).getPropertyValue("width"),
      10
    );
    let distanceToTranslate = direction * (width + gap);

    gsap.fromTo(
      payload.marque.children,
      { x: 0 },
      { x: distanceToTranslate, duration: 45, ease: "linear", repeat: -1 }
    );
  }
}

window.addEventListener("DOMContentLoaded", (event) => {
  new InfiniteMarque(
    { element: document.querySelectorAll(".js--marque-a") },
    -1
  );
  new InfiniteMarque(
    { element: document.querySelectorAll(".js--marque-b") },
    1
  );
});


const _header_burger = document.querySelector('#menu');
if (_header_burger) {

  var barTop = $('#bar-top');
  var barMidL = $('#bar-mid-l');
  var barMidR = $('#bar-mid-r');
  var barBottom = $('#bar-bottom');
  var circle = $('#circle');
  var menuIcon = $('#menu-icon');

  var tl = new TimelineLite({
    paused: true,
  });

  TweenLite.defaultEase = Expo.easeInOut; // Change the default easing

  tl
    .to(barTop, 0.2, {
      y: 5,
    })
    .to(barBottom, 0.2, {
      y: -5,
    }, 0)
    .to(barTop, 0.3, {
      rotation: 45,
      y: 0,
      x: 10,
      transformOrigin: "left top",
    }, 0.15)
    .to(barBottom, 0.3, {
      rotation: -45,
      y: 0,
      x: 10,
      transformOrigin: "left bottom",
    }, 0.15)
    .to(barMidL, 0.4, {
      opacity: 0,
      scaleX: 0,
      transformOrigin: "left",
    }, 0)
    .to(barMidR, 0.4, {
      opacity: 0,
      scaleX: 0,
      transformOrigin: "right",
    }, 0)
    .to(menuIcon, 0.8, {
      rotation: 90,
    }, 0.1)
    .fromTo(circle, 0.6, {
      opacity: 0,
      scale: 0,
      transformOrigin: "50% 50%",
      ease: Bounce.easeOut

    }, {
      scale: 1,
      opacity: 1,
      ease: Elastic.easeOut.config(1, 0.8),
    }, 0.5)
    .to([barTop, barBottom], 0.5, {
      scale: 0.6,
      transformOrigin: "50% 50%",
      ease: Power4.easeOut
    }, 0.5);


  $('#menu').click(function () {
    if ($(this).hasClass('toggled')) {
      tl.reverse();
    }
    else {
      tl.play();
    }
    $(this).toggleClass('toggled');
    $('.c-header__sp').toggleClass('show');
  });

  $('.c-header__sp nav ul li a').click(() => {
    $('#menu').toggleClass('toggled');
    $('.c-header__sp').toggleClass('show');
    tl.reverse();
  })
}

