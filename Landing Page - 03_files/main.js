let Obj = {};

(function ($) {
    "use strict";

    /************************************************************
     * Predefined letiables
     *************************************************************/
    let $window = $(window),
        $body = $('body'),
        $document = $(document);

    $.fn.exists = function () {
        return this.length > 0;
    };

    $.fn.isMobile = function () {
        if ($window.width() > 750) {
            return false;
        }
        return true;
    };
    let $header = document.querySelector("#header");
    let $btnScrollTop = document.querySelector(".c-footer .scrolltop");

    Obj.handelMenuClick = function () {
        let el = $('.c-header .p-buttonSp'),
            elMenu = '.c-header_sp',
            elButtonClose = '.c-header_sp .p-panelInfo_close';
        if (el.exists()) {
            el.bind('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if ($(this).hasClass('open')) {
                    $(this).removeClass('open')
                    $(elMenu).removeClass('open');
                    $body.removeClass('u-hidden');
                } else {
                    $(this).addClass('open')
                    $(elMenu).addClass('open');
                    $body.addClass('u-hidden');
                }
            }).closest('body').bind('click', function () {
                if (el.hasClass('open')) {
                    el.removeClass('open');
                    $(elMenu).removeClass('open');
                    $body.removeClass('u-hidden');
                }
            });
        }
        $(elMenu).on('click', function (e) {
            e.stopPropagation()
        })
        $(elButtonClose).bind('click', function (e) {
            if (el.hasClass('open')) {
                el.removeClass('open');
                $(elMenu).removeClass('open');
                $body.removeClass('u-hidden');
            }
        })
    }

    Obj.handelMenuClickInfo = function () {
        let el = $('#button_info')
        let elPanelInfo = '.p-panelInfo'
        let elButtonClose = '.p-panelInfo .p-panelInfo_close'
        if (el.exists()) {
            el.bind('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if ($(this).hasClass('open')) {
                    $(this).removeClass('open')
                    $(elPanelInfo).removeClass('open');
                    $body.removeClass('u-hidden');
                } else {
                    $(this).addClass('open')
                    $(elPanelInfo).addClass('open');
                }
            }).closest('body').bind('click', function () {
                if (el.hasClass('open')) {
                    el.removeClass('open');
                    $(elPanelInfo).addClass('open');
                    $(elPanelInfo).removeClass('open');
                }
            });
        }
        $('.p-panelInfo_wrap').on('click', function (e) {
            e.stopPropagation()
        })
        $(elButtonClose).bind('click', function (e) {
            if ($(elPanelInfo).hasClass('open')) {
                $(elPanelInfo).removeClass('open');
            }
        })
    }

    Obj.menuCategoryClick = function () {
        let el = $('.c-widget__archive__link');
        if (el.exists()) {
            el.click(function (e) {
                e.stopPropagation();
                let $this = $(this);
                if ($this.parent().hasClass('active')) {
                    $this.next().slideUp('fast', function () {
                        $this.parent().removeClass('active');
                    });
                } else {
                    $this.next().slideDown('fast', function () {
                        $this.parent().addClass('active');
                    });
                }
            });
        }
    }


    Obj.gotop = function () {
        let elGoTop = $('.c-btn__gotop');
        if (elGoTop.exists() && !elGoTop.isMobile()) {
            checkOffsetEL($window);

            $window.scroll(function () {
                checkOffsetEL($(this));
            });
            clickBtn();
        } else {
            clickBtn();
        }

        function checkOffsetEL($obj) {
            if ($obj.scrollTop() > 700) {
                elGoTop.fadeIn(300);
            } else {
                elGoTop.fadeOut(300);
            }
        }

        function clickBtn() {
            elGoTop.click(function () {
                $('body, html').animate({ scrollTop: 0 }, 500);
            });
        }
    }

    Obj.smoothAnchor = function () {
        if (window.location.hash) {
            var hash = window.location.hash.slice(1);
            $('html, body').animate({
                scrollTop: $('#' + hash).offset().top
            }, 300);
        };
        $('a[href*=\\#]:not([href=\\#])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $('#' + this.hash.slice(1));
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - 50
                    }, 1000);
                    return false;
                }
            }
        })
    }

    Obj.customScrollbar = function () {
        let el = $('.scrollbar-chrome');
        if (el.exists()) {
            el.scrollbar();
        }
    }

    Obj.initAnimationScroll = function () {
        let scrollOff = $('.scrollToggle'),
            windowsTop = $window.scrollTop(),
            wh = $window.height();
        if (scrollOff.exists()) {
            scrollOff.each(function () {
                let scrollOffTop = $(this).offset().top;
                $(this).addClass('ani--scrollOff');
                if (windowsTop + wh - 20 > scrollOffTop && $(this).hasClass('ani--scrollOff')) {
                    $(this).removeClass('ani--scrollOff').addClass('ani--scrollOn');
                } else {
                    $(this).removeClass('ani--scrollOn').addClass('ani--scrollOff');
                }
            });
        }
    }

    Obj.menuSpMulti = () => {
        $('.c-header_sp .menu .menu-item-has-children .sub-menu').before('<span class="js_toggle"><i class="fal fa-plus"></i></span>');
        $('.c-header_sp .menu .js_toggle').click(function () {
            $(this).prev().toggleClass('active')
            var $next = $(this).next();
            if (!$next.is(':animated')) $next.slideToggle(300).prev().toggleClass('active');
            if ($(this).hasClass("active")) {
                $(this).html('<i class="fal fa-minus"></i>');
            } else {
                $(this).html('<i class="fal fa-plus"></i>');
            }
        })
    }

    // BUTTON SCROLL TOP
    const handleBtnScrollTop = function () {
        $btnScrollTop.addEventListener("click", function () {
            $("html, body").animate({ scrollTop: 0 }, "slow");
        });
    };

    // SCROLL ADD STICKY HEADER
    window.addEventListener("scroll", function () {
        if (window.scrollY >= $header.clientHeight) {
            $header.classList.add("fixed");
            $btnScrollTop.classList.add("show");
            $('body').css('padding-top', $header.clientHeight);
            $('#menu-icon').addClass("white")
        } else {
            $header.classList.remove("fixed");
            $('#menu-icon').removeClass("white")
            $btnScrollTop.classList.remove("show");
            $('body').css('padding-top', 0);
        }
    });


    Obj.listFaqPage = () => {
        $('.listFaq .item .item_question').click(function () {
            $(this).toggleClass('active');
            $(this).next('.item_answer').slideToggle(300);
        })
    }

    Obj.heightItemBootCamp = () => {
        var maxHeight = 0;
        $('.c-page_home .c-section__sec04 .c-listBootcamp .item').each(function () {
            var itemHeight = $(this).outerHeight();
            if (itemHeight > maxHeight) {
                maxHeight = itemHeight + 40;
            }
        });
        $('.c-page_home .c-section__sec04 .c-listBootcamp .item').css('height', maxHeight + 'px');
    }


    /************************************************************
     * Obj Window load, ready, scroll, resize and functions
     *************************************************************/
    //Window load functions
    //
    $window.on('load', function () {

    });

    //Document ready functions
    $document.ready(function () {
        Obj.gotop();
        Obj.smoothAnchor();
        Obj.customScrollbar();
        Obj.handelMenuClick();
        Obj.menuCategoryClick();
        Obj.handelMenuClickInfo();
        Obj.initAnimationScroll();
        Obj.menuSpMulti();
        handleBtnScrollTop();
        Obj.listFaqPage();

        Obj.heightItemBootCamp();
    });

    //Window scroll functions
    $window.on('scroll', function () {
        Obj.initAnimationScroll();
    });

    //Window resize functions
    $window.on('resize', function () {

    });

})(jQuery);